<?php

namespace support;

/**
 * @method static void info($msg, string $type = 'info', array $context = [])
 * @method static void error($msg, string $type = 'error', array $context = [])
 * @method static void debug($msg, string $type = 'debug', array $context = [])
 * @method static void notice($msg, string $type = 'notice', array $context = [])
 * @method static void warning($msg, string $type = 'warning', array $context = [])
 * @method static void admin($msg, string $type = 'info', array $context = [])
 * @method static void api($msg, string $type = 'info', array $context = [])
 * @method static void request($msg, string $type = 'info', array $context = [])
 */
class Logger extends \WebmanTech\Logger\Logger
{
}
