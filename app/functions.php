<?php

use app\model\OptionModel;
use Shopwwi\LaravelCache\Cache;
use Webman\Captcha\CaptchaBuilder;

/**
 * 统一失败返回方法
 *
 * @param string $msg 提示消息
 * @param string $data 响应数据
 * @param array $other_result 其他一级数组数据
 * @param integer $code 错误码，默认0
 * @return void
 */
function error($msg = '', $data = '', $other_result = [], $code = 0)
{
    $result = [
        'code' => $code,
        'msg'  => $msg,
        'data' => $data,
    ];

    $result = array_merge($result, $other_result);
    return json($result);
}

/**
 * 统一成功返回方法
 *
 * @param string $msg 提示消息
 * @param string $data 相应数据
 * @param array $other_result 其他一级数组数据
 * @param integer $code 默认为1
 * @return void
 */
function success($msg = '', $data = '', $other_result = [], $code = 1)
{
    $result = [
        'code' => $code,
        'msg'  => $msg,
        'data' => $data,
    ];

    $result = array_merge($result, $other_result);
    return json($result);
}

/**
 * 获取配置option_value
 *
 * @param string $name option_name
 * @param boolean $is_update 是否更新缓存
 */
function get_option_value($name = '', $is_update = false){

    if(empty($name)) return [];

    //获取缓存
    $value = Cache::tags('option')->get('option_'.$name);
    if(empty($value) || $is_update){
        //查询数据
        $optionModel = new OptionModel();
        $option = $optionModel->where("option_name",$name)->find();
        
        if(!empty($option)){
            $value = $option->option_value;
            if(!empty($value['logo'])){
                $value['logo'] = get_image_url($value['logo']);
            }
            Cache::tags('option')->set('option_'.$name,$value,86400 * 30);
        }else{
            $value = [];
        }
    }

    return $value;
}

/**
 * 获取配置下的某个key
 *
 * @param [type] $name option_name
 * @param [type] $key option_value 里的下标key
 * @param string $default 默认内容
 * @return void
 */
function get_option_key($name,$key,$default='')
{
    $option = get_option_value($name);
    if(empty($option)) return $default;

    if(!isset($option[$key]) || empty($option[$key])) return $default;

    return $option[$key];
}

/**
 * 返回请求协议 http:// or https://
 */
function get_http_type()
{
    $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
    return $http_type;
}

/**
 * 获取带协议的请求域名
 */
function get_domain()
{
    return get_http_type() . request()->host();
}

/**
 * 转化图片的文件路径，为可以访问的url
 * @param string $file  文件路径，数据存储的文件相对路径
 * @return string 图片链接
 */
function get_image_url($file)
{
    if (empty($file)) {
        return '';
    }

    if (strpos($file, "http") === 0 || strpos($file, "https") === 0) {
        return $file;
    } else if (strpos($file, "/") === 0) {
        return get_domain() . $file;
    }

    return '';
}

/**
 * 获取当前管理员id
 */
function get_admin_id()
{
    $session = session();
    return $session->get('admin_id', null);
}

/**
 * 判断是否是post请求
 */
function is_post()
{
    $request = request();
    return $request->method() == 'POST' ? true : false;
}

/**
 * 判断是否是get请求
 */
function is_get()
{
    $request = request();
    return $request->method() == 'GET' ? true : false;
}

/**
 * 密码加密方法
 * @param string $pw   要加密的原始密码
 * @param string $salt 加密salt
 * @return string
 */
function get_password($pw, $salt = '')
{
    if (empty($salt)) {
        $salt = getenv('DB_PREFIX') ?? '';
    }
    $result = "###" . md5(md5($salt . $pw));
    return $result;
}

/**
 * 随机字符串生成
 * @param int $len 生成的字符串长度
 * @return string
 */
function get_random_string($len = 6)
{
    $chars    = [
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
        "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
        "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G",
        "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
        "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2",
        "3", "4", "5", "6", "7", "8", "9"
    ];
    $charsLen = count($chars) - 1;
    shuffle($chars);    // 将数组打乱
    $output = "";
    for ($i = 0; $i < $len; $i++) {
        $output .= $chars[mt_rand(0, $charsLen)];
    }
    return $output;
}

/**
 * 检查手机格式，中国手机不带国家代码，国际手机号格式为：国家代码-手机号
 * @param $mobile
 * @return bool
 */
function check_mobile($mobile)
{
    if (preg_match('/(^(13\d|14\d|15\d|16\d|17\d|18\d|19\d)\d{8})$/', $mobile)) {
        return true;
    } else {
        if (preg_match('/^\d{1,4}-\d{5,11}$/', $mobile)) {
            if (preg_match('/^\d{1,4}-0+/', $mobile)) {
                //不能以0开头
                return false;
            }

            return true;
        }

        return false;
    }
}

/**
 * 文件大小格式化
 * @param $bytes 文件大小（字节 Byte)
 * @return string
 */
function file_size_format($bytes)
{
    $type = ['B', 'KB', 'MB', 'GB', 'TB'];
    for ($i = 0; $bytes >= 1024; $i++) {
        $bytes /= 1024;
    }
    return (floor($bytes * 100) / 100) . $type[$i];
}

/**
 * 获取图形验证码
 *
 * @param string $name session存储key
 * @return string
 */
function get_captcha($key = 'captcha', $width = 114, $height = 42)
{
    // 初始化验证码类
    $builder = new CaptchaBuilder();
    // 生成验证码
    $builder->build($width, $height, null);
    // 将验证码的值存储到session中
    session()->set($key, strtolower($builder->getPhrase()));
    // 获得验证码图片二进制数据
    $img_content = $builder->get();
    // 返回验证码二进制数据
    return $img_content; //return response($img_content, 200, ['Content-Type' => 'image/jpeg']);
}
