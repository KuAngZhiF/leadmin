<?php

namespace app\model;

/**
 * 配置模型类
 */
class OptionModel extends BaseModel
{
    protected $name = 'option';
    
    // 设置json类型字段
	protected $json = ['option_value'];
    
    // 设置JSON数据返回数组
    protected $jsonAssoc = true;
    
}
