<?php

namespace app\model;


/**
 * 上传文件记录表
 */
class UploadFileModel extends BaseModel
{
    protected $name = 'upload_file';

    static function upload($data = [])
    {

        if (!$data['unique_id'] || !$data['size']) {
            return false;
        }

        if (self::where("unique_id", $data['unique_id'])->count() > 0) {
            return false;
        }

        self::create($data);
        return true;
    }
}
