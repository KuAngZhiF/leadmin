# LeAdmin 后台框架

[![Latest Stable Version](http://poser.pugx.org/le/leadmin/v)](https://packagist.org/packages/le/leadmin) [![Total Downloads](http://poser.pugx.org/le/leadmin/downloads)](https://packagist.org/packages/le/leadmin) [![Latest Unstable Version](http://poser.pugx.org/le/leadmin/v/unstable)](https://packagist.org/packages/le/leadmin) [![License](http://poser.pugx.org/le/leadmin/license)](https://packagist.org/packages/le/leadmin) [![PHP Version Require](http://poser.pugx.org/le/leadmin/require/php)](https://packagist.org/packages/le/leadmin)

#### 介绍
LeAdmin 使用基于workerman的webman http框架开发<br/>
相比较于之前的[webman + pearadmin 后台管理框架](https://gitee.com/yjlkyzg/webmanadmin/tree/master) 主要要求php版本8.0及以上，并且代码上针对原来的后台进行简化并统一格式<br/>

[点击查看LeAdmin演示地址](http://leadmin.itjiale.com/admin)
admin 123456 (请勿修改管理员密码)

#### 软件架构
1.  后端使用基于workerman的webman http常驻内存框架 + Php8.0 + Mysql8.0 + Redis
2.  使用Think-orm 3.0版本(基于8.0重构)
2.  前端使用pear admin layui 版本

#### 开发说明

1.  使用success() error() 统一返回成功、失败相应 方法在app\function.php中
2.  使用Logger类进行日志记录,默认设置info、error、debug、admin、api、request目录,例:Logger::info('测试日志信息')
3.  新增统一异常处理类 support/ExceptionHandler.php 统一返回json错误信息，记录日志到error目录下
4.  app/exception为自定义的异常类，可根据业务增加
5.  app/middleware为中间件目录,Access.php为全局中间件，处理跨域等
6.  使用webman/log记录请求日志,默认关闭,可在.env里配置启用,会记录所有请求数据以及sql等,生产务必要关闭，否则会产生大量日志文件
7.  function.php 部分方法, get_image_url 获取可访问的文件路径 get_option_value 获取配置数组 get_option_key 获取配置下某个key get_admin_id 获取当前管理员id
8.  目前后台缓存数据，管理员信息、角色的菜单ids、角色的菜单列表、系统配置数据


开发说明后续持续更新...



#### 使用组件(开发中有问题可查看对应组件文档)

1.  [webman/think-orm 数据库组件](https://www.workerman.net/doc/webman/db/thinkorm.html)
2.  [illuminate/redis laravel redis](https://www.workerman.net/doc/webman/db/redis.html)
3.  [vlucas/phpdotenv env环境变量组件](https://www.workerman.net/doc/webman/components/env.html)
4.  [workerman/validation 验证器](https://www.workerman.net/doc/webman/components/validation.html)
5.  [webman/captcha 验证码](https://www.workerman.net/doc/webman/components/captcha.html)
6.  [workerman/crontab 定时任务](https://www.workerman.net/doc/webman/components/crontab.html)
7.  [phpoffice/phpspreadsheet Excel表格](https://www.workerman.net/doc/webman/components/excel.html)
8.  [webman/redis-queue redis队列](https://www.workerman.net/plugin/12)
9.  [webman/event event事件](https://www.workerman.net/plugin/64)
10.  [webman-tech/logger 日志统筹化(分目录生成日志)](https://www.workerman.net/plugin/58)
11.  [shopwwi/laravel-cache laravel缓存](https://www.workerman.net/plugin/95)
12.  [tinywan/storage 文件上传(本地、阿里云、腾讯云、七牛云)](https://www.workerman.net/plugin/21)
13.  [topthink/think-template Thinkphp模板引擎](https://www.kancloud.cn/manual/think-template/1286403)
14.  [webman/log 日志记录(默认关闭，开启请在.env里配置)](https://www.workerman.net/plugin/61)


#### 安装教程

1.  安装环境，可使用宝塔安装，Php8.0 + Mysql8.0 + Redis , php安装redis扩展，删除宝塔禁用函数
2.  使用composer 安装 composer require le/leadmin
3.  导入/sql 文件
4.  复制.env.example 并重命名为.env,修改里面的mysql、redis配置
5.  项目根目录使用 php start.php start 启动 加上 -d 则为保持后台运行(首次建议不加-d 可查看是否有报错)

##### V1.0
1.  完成基本功能,后台登录、系统设置、菜单管理、角色管理、管理员管理、修改密码、图片上传、附件上传列表


#### 部分截图
![输入图片说明](https://www.itjiale.com/images/1.png)
![输入图片说明](https://www.itjiale.com/images/2.png)
![输入图片说明](https://www.itjiale.com/images/3.png)
![输入图片说明](https://www.itjiale.com/images/4.png)
![输入图片说明](https://www.itjiale.com/images/5.png)