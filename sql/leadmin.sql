/*
 Navicat Premium Data Transfer

 Source Server Type    : MySQL
 Source Server Version : 80024 (8.0.24)
 Source Schema         : leadmin

 Target Server Type    : MySQL
 Target Server Version : 80024 (8.0.24)
 File Encoding         : 65001

*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for la_admin
-- ----------------------------
DROP TABLE IF EXISTS `la_admin`;
CREATE TABLE `la_admin` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录账号',
  `password` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录密码',
  `status` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '状态 0:禁用 1:启用',
  `roles` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色',
  `last_login_ip` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '最后登录id',
  `last_login_time` bigint unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `create_time` bigint unsigned NOT NULL DEFAULT '0',
  `update_time` bigint unsigned NOT NULL DEFAULT '0',
  `delete_time` bigint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_account` (`account`,`delete_time`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='管理员表';

-- ----------------------------
-- Records of la_admin
-- ----------------------------
BEGIN;
INSERT INTO `la_admin` (`id`, `account`, `password`, `status`, `roles`, `last_login_ip`, `last_login_time`, `create_time`, `update_time`, `delete_time`) VALUES (1, 'admin', '###6f1de257b1de091a152cfcaa5bed255f', 1, '1', '0.0.0.0', 1680959826, 1680959307, 1680959826, 0);
INSERT INTO `la_admin` (`id`, `account`, `password`, `status`, `roles`, `last_login_ip`, `last_login_time`, `create_time`, `update_time`, `delete_time`) VALUES (2, 'test1', '###6f1de257b1de091a152cfcaa5bed255f', 1, '2', '0.0.0.0', 1680965156, 1680965123, 1680965156, 0);
COMMIT;

-- ----------------------------
-- Table structure for la_admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `la_admin_menu`;
CREATE TABLE `la_admin_menu` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0' COMMENT '上级菜单,顶级为0',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单名称',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图标',
  `type` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '类型0:目录 1:菜单',
  `app` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `controller` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `is_show` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '是否显示 1:显示 0:隐藏',
  `sort` int unsigned NOT NULL DEFAULT '0' COMMENT '排序 大->小',
  `create_time` bigint unsigned NOT NULL DEFAULT '0',
  `update_time` bigint unsigned NOT NULL DEFAULT '0',
  `delete_time` bigint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_search` (`action`,`controller`,`app`,`delete_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='后台菜单表';

-- ----------------------------
-- Records of la_admin_menu
-- ----------------------------
BEGIN;
INSERT INTO `la_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `create_time`, `update_time`, `delete_time`) VALUES (1, 0, '系统管理', 'layui-icon-util', 0, 'admin', 'system', 'default', 1, 9999, 1680873247, 1680873247, 0);
INSERT INTO `la_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `create_time`, `update_time`, `delete_time`) VALUES (2, 1, '系统设置', '', 1, 'admin', 'system', 'setting', 1, 0, 1680873247, 1680873247, 0);
INSERT INTO `la_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `create_time`, `update_time`, `delete_time`) VALUES (3, 1, '菜单管理', '', 1, 'admin', 'system', 'menu', 1, 0, 1680873247, 1680873247, 0);
INSERT INTO `la_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `create_time`, `update_time`, `delete_time`) VALUES (4, 1, '角色管理', '', 1, 'admin', 'system', 'roles', 1, 0, 1680873247, 1680873247, 0);
INSERT INTO `la_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `create_time`, `update_time`, `delete_time`) VALUES (5, 1, '管理员管理', '', 1, 'admin', 'system', 'admin', 1, 0, 1680873247, 1680873247, 0);
INSERT INTO `la_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `create_time`, `update_time`, `delete_time`) VALUES (6, 3, '获取菜单列表', '', 1, 'admin', 'system', 'getMenuList', 0, 0, 1680962644, 1680962644, 0);
INSERT INTO `la_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `create_time`, `update_time`, `delete_time`) VALUES (7, 3, '添加/编辑 菜单', '', 1, 'admin', 'system', 'editMenu', 0, 0, 1680962671, 1680962671, 0);
INSERT INTO `la_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `create_time`, `update_time`, `delete_time`) VALUES (8, 3, '删除菜单', '', 1, 'admin', 'system', 'delMenu', 0, 0, 1680962847, 1680962853, 0);
INSERT INTO `la_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `create_time`, `update_time`, `delete_time`) VALUES (9, 4, '获取角色列表', '', 1, 'admin', 'system', 'getRolesList', 0, 0, 1680966231, 1680966231, 0);
INSERT INTO `la_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `create_time`, `update_time`, `delete_time`) VALUES (10, 4, '添加/编辑 角色', '', 1, 'admin', 'system', 'editRoles', 0, 0, 1680966251, 1680966251, 0);
INSERT INTO `la_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `create_time`, `update_time`, `delete_time`) VALUES (11, 4, '删除角色', '', 1, 'admin', 'system', 'delRoles', 0, 0, 1680966273, 1680966273, 0);
INSERT INTO `la_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `create_time`, `update_time`, `delete_time`) VALUES (12, 5, '获取管理员列表', '', 1, 'admin', 'system', 'getAdminList', 0, 0, 1680966293, 1680966293, 0);
INSERT INTO `la_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `create_time`, `update_time`, `delete_time`) VALUES (13, 5, '添加/编辑 管理员', '', 1, 'admin', 'system', 'editAdmin', 0, 0, 1680966315, 1680966315, 0);
INSERT INTO `la_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `create_time`, `update_time`, `delete_time`) VALUES (14, 5, '修改管理员状态', '', 1, 'admin', 'system', 'updAdminStatus', 0, 0, 1680966336, 1680966336, 0);
INSERT INTO `la_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `create_time`, `update_time`, `delete_time`) VALUES (15, 5, '删除管理员', '', 1, 'admin', 'system', 'delAdmin', 0, 0, 1680966356, 1680966356, 0);
INSERT INTO `la_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `create_time`, `update_time`, `delete_time`) VALUES (16, 1, '附件上传记录', '', 1, 'admin', 'system', 'uploadFile', 1, 0, 1680966379, 1680966379, 0);
INSERT INTO `la_admin_menu` (`id`, `pid`, `title`, `icon`, `type`, `app`, `controller`, `action`, `is_show`, `sort`, `create_time`, `update_time`, `delete_time`) VALUES (17, 16, '获取附件上传记录', '', 1, 'admin', 'system', 'getUploadFile', 0, 0, 1680966410, 1680966410, 0);
COMMIT;

-- ----------------------------
-- Table structure for la_admin_roles
-- ----------------------------
DROP TABLE IF EXISTS `la_admin_roles`;
CREATE TABLE `la_admin_roles` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `rules` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '角色菜单ids',
  `create_time` bigint unsigned NOT NULL DEFAULT '0',
  `update_time` bigint unsigned NOT NULL DEFAULT '0',
  `delete_time` bigint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='管理员角色表';

-- ----------------------------
-- Records of la_admin_roles
-- ----------------------------
BEGIN;
INSERT INTO `la_admin_roles` (`id`, `name`, `rules`, `create_time`, `update_time`, `delete_time`) VALUES (1, '超级管理员', '*', 1680961049, 1680961049, 0);
INSERT INTO `la_admin_roles` (`id`, `name`, `rules`, `create_time`, `update_time`, `delete_time`) VALUES (2, '菜单管理', '1,3,6,7,8', 1680963676, 1680963676, 0);
COMMIT;

-- ----------------------------
-- Table structure for la_option
-- ----------------------------
DROP TABLE IF EXISTS `la_option`;
CREATE TABLE `la_option` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `option_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `create_time` bigint unsigned NOT NULL DEFAULT '0',
  `update_time` bigint unsigned NOT NULL DEFAULT '0',
  `delete_time` bigint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of la_option
-- ----------------------------
BEGIN;
INSERT INTO `la_option` (`id`, `option_name`, `option_value`, `create_time`, `update_time`, `delete_time`) VALUES (1, 'site_config', '{\"title\":\"LeAdmin\\u540e\\u53f0\\u7ba1\\u7406\",\"desc\":\"LeAdmin\\u540e\\u53f0\\u7ba1\\u7406\",\"logo\":\"\"}', 1680965680, 1680965680, 0);
INSERT INTO `la_option` (`id`, `option_name`, `option_value`, `create_time`, `update_time`, `delete_time`) VALUES (2, 'upload_config', '{\"type\":\"0\",\"extensions\":\"jpg,jpeg,png,gif,bmp4,mp4,avi,wmv,rm,rmvb,mkv,mp3,wma,wav,txt,pdf,doc,docx,xls,xlsx,ppt,pptx,zip,rar,csv\",\"maxsize\":\"50\"}', 1680965680, 1680965680, 0);
COMMIT;

-- ----------------------------
-- Table structure for la_upload_file
-- ----------------------------
DROP TABLE IF EXISTS `la_upload_file`;
CREATE TABLE `la_upload_file` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `origin_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '原始文件名',
  `save_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '保存文件名',
  `save_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '绝对路径',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '访问路径',
  `unique_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'uniqid',
  `size` int unsigned NOT NULL DEFAULT '0' COMMENT '大小(字节)',
  `mime_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型',
  `extension` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '后缀',
  `create_time` bigint unsigned NOT NULL DEFAULT '0',
  `update_time` bigint unsigned NOT NULL DEFAULT '0',
  `delete_time` bigint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_unique_id` (`unique_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='文件上传记录表';

-- ----------------------------
-- Records of la_upload_file
-- ----------------------------
BEGIN;
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
