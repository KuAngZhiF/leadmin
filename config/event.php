<?php

use app\model\AdminModel;

return [
    'admin.admin_upd_cache' => [//更新管理员缓存
        function($data){
            AdminModel::getDetail($data,true);
        }
    ],
];
